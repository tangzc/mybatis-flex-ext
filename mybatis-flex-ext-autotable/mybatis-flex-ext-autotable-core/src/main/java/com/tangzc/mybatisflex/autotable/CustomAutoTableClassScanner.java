package com.tangzc.mybatisflex.autotable;

import com.mybatisflex.annotation.Table;
import org.dromara.autotable.core.AutoTableClassScanner;

import java.lang.annotation.Annotation;
import java.util.Set;

public class CustomAutoTableClassScanner extends AutoTableClassScanner {
    @Override
    protected Set<Class<? extends Annotation>> getIncludeAnnotations() {
        Set<Class<? extends Annotation>> includeAnnotations = super.getIncludeAnnotations();
        includeAnnotations.add(Table.class);
        return includeAnnotations;
    }
}
