package com.tangzc.mybatisflex.autotable;

import com.mybatisflex.spring.boot.MybatisFlexAutoConfiguration;
import com.mybatisflex.spring.boot.MybatisFlexProperties;
import org.dromara.autotable.core.AutoTableClassScanner;
import org.dromara.autotable.core.AutoTableMetadataAdapter;
import org.dromara.autotable.core.converter.JavaTypeToDatabaseTypeConverter;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author don
 */
@Configuration
@AutoConfigureAfter(MybatisFlexAutoConfiguration.class)
public class MybatisFlexAutoTableAutoConfig {

    @Bean
    public AutoTableMetadataAdapter customAutoTableMetadataAdapter(MybatisFlexProperties mybatisFlexProperties) {
        return new CustomAutoTableMetadataAdapter(mybatisFlexProperties);
    }

    @Bean
    public AutoTableClassScanner customAutoTableClassScanner() {
        return new CustomAutoTableClassScanner();
    }

    @Bean
    public JavaTypeToDatabaseTypeConverter customJavaTypeToDatabaseTypeConverter() {
        return new CustomJavaTypeToDatabaseTypeConverter();
    }

    @Bean
    public AutoTableDynamicDatasourceHandler dynamicDatasourceHandler() {
        return new AutoTableDynamicDatasourceHandler();
    }
}
