package com.tangzc.mybatisflex.autotable;

import com.mybatisflex.annotation.Column;
import org.apache.ibatis.type.UnknownTypeHandler;
import org.dromara.autotable.core.converter.JavaTypeToDatabaseTypeConverter;
import org.springframework.core.annotation.AnnotatedElementUtils;

import java.lang.reflect.Field;

public class CustomJavaTypeToDatabaseTypeConverter implements JavaTypeToDatabaseTypeConverter {

    @Override
    public Class<?> getFieldType(Class<?> clazz, Field field) {
        // 枚举，按照字符串处理
        if (field.getType().isEnum()) {
            return String.class;
        }
        Column column = AnnotatedElementUtils.findMergedAnnotation(field, Column.class);
        // json数据，按照字符串处理
        if (column != null && column.typeHandler() != UnknownTypeHandler.class) {
            return String.class;
        }

        return field.getType();
    }
}
