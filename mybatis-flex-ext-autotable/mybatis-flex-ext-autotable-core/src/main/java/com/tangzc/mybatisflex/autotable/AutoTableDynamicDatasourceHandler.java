package com.tangzc.mybatisflex.autotable;

import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.datasource.DataSourceKey;
import org.dromara.autotable.core.dynamicds.IDataSourceHandler;
import lombok.NonNull;
import org.springframework.core.annotation.AnnotatedElementUtils;

/**
 * 多数据源模式
 *
 * @author don
 */
public class AutoTableDynamicDatasourceHandler implements IDataSourceHandler {

    @Override
    public void useDataSource(String dsName) {
        DataSourceKey.use(dsName);
    }

    @Override
    public void clearDataSource(String serializable) {
        DataSourceKey.clear();
    }

    @NonNull
    @Override
    public String getDataSourceName(Class clazz) {
        Table tableAnno = AnnotatedElementUtils.findMergedAnnotation(clazz, Table.class);
        if (tableAnno != null) {
            return tableAnno.dataSource();
        }
        return "";
    }
}
