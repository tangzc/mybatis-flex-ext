<p align="center"><img src="https://s2.loli.net/2024/03/25/6CX75MawVsUgIth.png" alt="1648883788444-1068117e-f573-4b0b-bbb9-8a3208810860.png" width="150px" /></p>



<p align="center">助力MybatisFlex，让MybatisFlex飞的更快</p>

<p align="center">
<img src="https://img.shields.io/badge/license-Apache 2-4EB1BA.svg?style=for-the-badge" alt="img" />
</p>



&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;尽管[MybatisFlex](https://gitee.com/mybatis-flex/mybatis-flex)
（后文简称MF）相比较Mybatis丝滑了很多，但是，日常使用中，是否偶尔仍会怀念JPA（Hibernate）的那种纵享丝滑的感受，更好的一心投入业务开发中，如果你也是如此，那么恭喜你发现了MybatisFlexExt（后文简称MFE）。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MFE对MF做了进一步的拓展封装，即保留MF原功能，又添加更多有用便捷的功能。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;增强功能具体体现在几个方面：自动建表(<a href="https://gitee.com/tangzc/auto-table" target="_blank">auto-table</a>)、数据自动填充（类似JPA中的审计）等功能做了补充完善。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;如果感觉本框架对您有所帮助，烦请去[Gitee](https://gitee.com/tangzc/mybatis-flex-ext)
给个小星星⭐️，欢迎来撩共同进步。

<p align="center"><img src="https://s2.loli.net/2022/04/02/Sc6uMsaKNY9nWBE.png" alt="img" width="200px" /></p>

## 关联推荐

<a style="font-size:20px" href="https://autotable.tangzc.com/" target="_blank">AutoTable</a>

## 特别感谢

> 感谢JetBrains提供的软件支持

<img width="200" src="https://resources.jetbrains.com/storage/products/company/brand/logos/jb_beam.png" alt="JetBrains Logo (Main) logo.">
