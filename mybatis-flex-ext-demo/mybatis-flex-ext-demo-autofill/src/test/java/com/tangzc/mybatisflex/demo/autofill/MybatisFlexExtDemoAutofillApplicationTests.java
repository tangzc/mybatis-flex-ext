package com.tangzc.mybatisflex.demo.autofill;

import org.dromara.autotable.springboot.EnableAutoTableTest;
import com.tangzc.mybatisflex.demo.autofill.entity.TestEntity;
import com.tangzc.mybatisflex.demo.autofill.mapper.TestEntityMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

@EnableAutoTableTest
@SpringBootTest
public class MybatisFlexExtDemoAutofillApplicationTests {

    @Resource
    private TestEntityMapper testEntityMapper;

    private static final String ID = "1";

    @Test
    public void insert() {
        TestEntity testEntity = new TestEntity();
        testEntity.setId(ID);
        testEntity.setName("test");
        testEntityMapper.insert(testEntity);
        assert testEntity.getCreateTime() != null;
        assert testEntity.getModifyTime() != null;
    }

    @Test
    public void update() throws InterruptedException {
        if(testEntityMapper.selectOneById(ID) == null) {
            insert();
        }
        TimeUnit.SECONDS.sleep(1);

        TestEntity testEntity = testEntityMapper.selectOneById("1");
        testEntity.setName("lise");
        LocalDateTime oldCreateTime = testEntity.getCreateTime();
        LocalDateTime oldModifyTime = testEntity.getModifyTime();
        testEntityMapper.update(testEntity);

        assert oldCreateTime.equals(testEntity.getCreateTime());
        assert oldModifyTime.isBefore(testEntity.getModifyTime());
    }
}
