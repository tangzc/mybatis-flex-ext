package com.tangzc.mybatisflex.demo.autofill.entity;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import org.dromara.autotable.annotation.AutoTable;
import com.tangzc.mybatisflex.annotation.InsertFillTime;
import com.tangzc.mybatisflex.annotation.InsertUpdateFillTime;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Accessors(chain = true)
@Table(value = "test_entity", comment = "测试实体")
@AutoTable
@Data
public class TestEntity {

    @Id
    private String id;

    private String name;

    @InsertFillTime
    private LocalDateTime createTime;

    @InsertUpdateFillTime
    private LocalDateTime modifyTime;
}
