package com.tangzc.mybatisflex.demo.autofill;

import org.dromara.autotable.springboot.EnableAutoTable;
import com.tangzc.mybatisflex.demo.autofill.entity.TestEntity;
import com.tangzc.mybatisflex.demo.autofill.mapper.TestEntityMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

@EnableAutoTable
@SpringBootApplication
public class MybatisFlexExtDemoAutofillApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisFlexExtDemoAutofillApplication.class, args);
    }
}
