package com.tangzc.mybatisflex.core;

import java.lang.reflect.Field;

public interface FieldTypeHandler {
    Class<?> getDateType(Class<?> clazz, Field field);
}
