package com.tangzc.mybatisflex;

import com.mybatisflex.core.FlexGlobalConfig;
import com.tangzc.mybatisflex.core.EntityWriteListener;
import com.tangzc.mybatisflex.core.MybatisFlexGlobalConfig;
import com.tangzc.mybatisflex.util.SpringContextUtil;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author don
 */
@Configuration
@EnableConfigurationProperties(MybatisFlexGlobalConfig.class)
public class MybatisFlexAutoConfig {

    public MybatisFlexAutoConfig(MybatisFlexGlobalConfig globalConfig) {
        if (globalConfig.getEnableAutoFill()) {
            FlexGlobalConfig defaultConfig = FlexGlobalConfig.getDefaultConfig();
            defaultConfig.registerInsertListener(new EntityWriteListener(), Object.class);
            defaultConfig.registerUpdateListener(new EntityWriteListener(), Object.class);
            FlexGlobalConfig.setDefaultConfig(defaultConfig);
        }
    }

    @Bean
    public SpringContextUtil springContextUtil() {
        return new SpringContextUtil();
    }
}
