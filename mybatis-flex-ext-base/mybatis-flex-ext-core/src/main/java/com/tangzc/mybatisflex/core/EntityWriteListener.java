package com.tangzc.mybatisflex.core;

import com.mybatisflex.annotation.InsertListener;
import com.mybatisflex.annotation.UpdateListener;
import com.tangzc.mybatisflex.annotation.DefaultValue;
import com.tangzc.mybatisflex.annotation.FieldFill;
import com.tangzc.mybatisflex.annotation.FillData;
import com.tangzc.mybatisflex.annotation.FillTime;
import com.tangzc.mybatisflex.annotation.handler.AutoFillHandler;
import com.tangzc.mybatisflex.util.SpringContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.core.annotation.AnnotatedElementUtils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
public class EntityWriteListener implements InsertListener, UpdateListener {

    /**
     * 基本类型与包装类型对应关系
     */
    public Map<Class<?>, Class<?>> baseTypeClassMap = new HashMap<>();

    {
        baseTypeClassMap.put(Byte.class, byte.class);
        baseTypeClassMap.put(Short.class, short.class);
        baseTypeClassMap.put(Integer.class, int.class);
        baseTypeClassMap.put(Long.class, long.class);
        baseTypeClassMap.put(Double.class, double.class);
        baseTypeClassMap.put(Float.class, float.class);
        baseTypeClassMap.put(Character.class, char.class);
        baseTypeClassMap.put(Boolean.class, boolean.class);
    }

    /**
     * 需要处理的字段的缓存列表
     */
    private static final Map<Class<?>, List<Field>> FIELD_LIST_CATCH_MAP = new ConcurrentHashMap<>();

    @Override
    public void onInsert(Object entity) {

        Class<?> clazz = entity.getClass();
        List<Field> fieldList = getFieldList(clazz);

        if (!fieldList.isEmpty()) {
            fill(FieldFill.INSERT, entity, clazz, fieldList);
        }
    }

    @Override
    public void onUpdate(Object entity) {

        Class<?> clazz = entity.getClass();
        List<Field> fieldList = getFieldList(clazz);

        if (!fieldList.isEmpty()) {
            fill(FieldFill.UPDATE, entity, clazz, fieldList);
        }
    }

    public void fill(FieldFill fill, Object object, Class<?> clazz, List<Field> fieldList) {

        Now now = new Now();

        fieldList.forEach(field -> {

            // 默认值
            setDefaultVale(fill, object, clazz, field);

            // 操作时间
            setOptionDate(fill, object, clazz, field, now);

            // 操作人
            setOptionUser(fill, object, clazz, field);
        });
    }

    private void setDefaultVale(FieldFill fill, Object object, Class<?> clazz, Field field) {

        DefaultValue defaultValue = AnnotatedElementUtils.getMergedAnnotation(field, DefaultValue.class);
        if (defaultValue == null || (defaultValue.fill() != fill && defaultValue.fill() != FieldFill.INSERT_UPDATE)) {
            return;
        }

        BeanWrapper beanWrapper = new BeanWrapperImpl(object);
        String fieldName = field.getName();
        boolean canSet = beanWrapper.getPropertyValue(fieldName) == null;
        if (canSet) {
            Object newVal = convert(field, defaultValue);
            beanWrapper.setPropertyValue(fieldName, newVal);
        }
    }

    private void setOptionUser(FieldFill fill, Object object, Class<?> clazz, Field field) {

        FillData fillData = AnnotatedElementUtils.getMergedAnnotation(field, FillData.class);
        if (fillData == null || (fillData.fill() != fill && fillData.fill() != FieldFill.INSERT_UPDATE)) {
            return;
        }

        BeanWrapper beanWrapper = new BeanWrapperImpl(object);
        // 判断原来值为null，或者覆盖选项为true
        String fieldName = field.getName();
        boolean canSet = fillData.override() || beanWrapper.getPropertyValue(fieldName) == null;

        if (canSet) {

            Object userInfo = null;

            AutoFillHandler instance = getAutoFillHandler(fillData.value());
            if (instance != null) {
                userInfo = instance.getVal(object, clazz, field);
            }

            // 如果当前未取到信息，不设置
            if (userInfo != null) {
                // 先校验类型是否一致
                if (!this.checkTypeConsistency(userInfo.getClass(), field.getType())) {
                    String errorMsg = clazz.getName() + "中的字段" + fieldName + "的类型（" + field.getType() + "）与" + instance.getClass() + "返回值的类型（" + userInfo.getClass() + "）不一致";
                    throw new RuntimeException(errorMsg);
                }
                // 赋值
                beanWrapper.setPropertyValue(fieldName, userInfo);
            }
        }
    }

    private static List<Field> getFieldList(Class<?> clazz) {

        return FIELD_LIST_CATCH_MAP.computeIfAbsent(clazz, $ -> {

            Field[] declaredFields = clazz.getDeclaredFields();
            List<Field> fields = new ArrayList<>(Arrays.asList(declaredFields));
            for (Class<?> superClass = clazz.getSuperclass(); superClass != null; superClass = superClass.getSuperclass()) {
                declaredFields = superClass.getDeclaredFields();
                fields.addAll(Arrays.asList(declaredFields));
            }

            return fields.stream()
                    .filter(field -> AnnotatedElementUtils.hasMetaAnnotationTypes(field, FillTime.class) ||
                            AnnotatedElementUtils.hasMetaAnnotationTypes(field, FillData.class) ||
                            AnnotatedElementUtils.hasMetaAnnotationTypes(field, DefaultValue.class))
                    .collect(Collectors.toList());
        });
    }

    /**
     * 校验两个类型是否一致，涵盖了8大基本类型的自动转换
     */
    private boolean checkTypeConsistency(Class<?> aClass, Class<?> bClass) {
        return aClass == bClass ||
                baseTypeClassMap.get(aClass) == bClass ||
                baseTypeClassMap.get(bClass) == aClass;
    }

    /**
     * 缓存AutoFillHandler，同时寻找
     */
    private AutoFillHandler getAutoFillHandler(Class<? extends AutoFillHandler> autoFillHandler) {

        try {
            return SpringContextUtil.getBeanOfType(autoFillHandler);
        } catch (NoUniqueBeanDefinitionException ignore) {
            throw new RuntimeException("发现了多个" + autoFillHandler.getName() + "的实现，请保持spring中只有一个实例。");
        } catch (NoSuchBeanDefinitionException ignore) {
            if (autoFillHandler.isInterface()) {
                log.warn("没有找到{}的实现，操作人信息无法自动填充。", autoFillHandler.getName());
            } else {
                log.warn("{}需要注册到spring，不然操作人信息无法自动填充。", autoFillHandler.getName());
            }
        }
        return null;
    }

    private void setOptionDate(FieldFill fill, Object object, Class<?> clazz, Field field, Now now) {

        FillTime fillTime = AnnotatedElementUtils.getMergedAnnotation(field, FillTime.class);
        if (fillTime == null || (fillTime.fill() != fill && fillTime.fill() != FieldFill.INSERT_UPDATE)) {
            return;
        }

        BeanWrapper beanWrapper = new BeanWrapperImpl(object);
        String fieldName = field.getName();
        boolean canSet = fillTime.override() || beanWrapper.getPropertyValue(fieldName) == null;

        if (canSet) {
            Class<?> type = getDateType(clazz, field);

            Object nowDate = Optional.ofNullable(now.now(type, fillTime.format()))
                    .orElseThrow(() -> new RuntimeException("类：" + clazz.toString() + "的字段：" + fieldName
                            + "的类型不支持。仅支持String、Long、long、Date、LocalDate、LocalDateTime"));

            // 赋值
            beanWrapper.setPropertyValue(fieldName, nowDate);
        }
    }

    /**
     * 获取日期类字段的类型
     */
    private Class<?> getDateType(Class<?> clazz, Field field) {

        Class<?> type = field.getType();
        try {
            FieldTypeHandler fieldTypeHandler = SpringContextUtil.getBeanOfType(FieldTypeHandler.class);
            type = fieldTypeHandler.getDateType(clazz, field);
        } catch (BeansException ignore) {
        }

        return type;
    }

    /**
     * 默认值转化
     *
     * @param field        默认值字段
     * @param defaultValue 默认值注解
     * @return 默认值
     */
    private Object convert(Field field, DefaultValue defaultValue) {

        String value = defaultValue.value();
        String format = defaultValue.format();
        Class<?> type = field.getType();
        Map<Class<?>, Function<String, Object>> convertFuncMap = new HashMap<Class<?>, Function<String, Object>>(16) {{
            put(String.class, value -> value);
            put(Long.class, Long::parseLong);
            put(long.class, Long::parseLong);
            put(Integer.class, Integer::parseInt);
            put(int.class, Integer::parseInt);
            put(Boolean.class, Boolean::parseBoolean);
            put(boolean.class, Boolean::parseBoolean);
            put(Double.class, Double::parseDouble);
            put(double.class, Double::parseDouble);
            put(Float.class, Float::parseFloat);
            put(float.class, Float::parseFloat);
            put(BigDecimal.class, BigDecimal::new);
            put(Date.class, value -> {
                try {
                    return new SimpleDateFormat(format).parse(value);
                } catch (ParseException e) {
                    throw new RuntimeException("日期格式" + format + "与值" + value + "不匹配！");
                }
            });
            put(LocalDate.class, value -> {
                try {
                    return LocalDate.parse(value, DateTimeFormatter.ofPattern(format));
                } catch (Exception e) {
                    throw new RuntimeException("日期格式" + format + "与值" + value + "不匹配！");
                }
            });
            put(LocalDateTime.class, value -> {
                try {
                    return LocalDateTime.parse(value, DateTimeFormatter.ofPattern(format));
                } catch (Exception e) {
                    throw new RuntimeException("日期格式" + format + "与值" + value + "不匹配！");
                }
            });
        }};

        Function<String, Object> convertFunc = convertFuncMap.getOrDefault(type, val -> {
            if (type.isEnum()) {
                Object[] enumConstants = type.getEnumConstants();
                if (enumConstants.length > 0) {
                    for (Object enumConstant : enumConstants) {
                        if (Objects.equals(val, enumConstant.toString())) {
                            return enumConstant;
                        }
                    }
                }
                throw new RuntimeException("默认值" + val + "与枚举" + type.getName() + "不匹配！");
            } else {
                return val;
            }
        });
        return convertFunc.apply(value);
    }

    /**
     * 框架内部使用，定义当前时间的时间对象
     */
    private static class Now {

        private final LocalDateTime localDateTime = LocalDateTime.now();
        private final LocalDate localDate = this.localDateTime.toLocalDate();
        private final Date date = Date.from(this.localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        private final Long timestamp = this.date.getTime();

        public Object now(Class<?> type, String format) {

            if (type == String.class) {
                return this.localDateTime.format(DateTimeFormatter.ofPattern(format));
            }

            if (type == long.class || type == Long.class) {
                return timestamp;
            }

            if (type == Date.class) {
                return date;
            }

            if (type == LocalDate.class) {
                return localDate;
            }

            if (type == LocalDateTime.class) {
                return localDateTime;
            }

            return null;
        }
    }

}
