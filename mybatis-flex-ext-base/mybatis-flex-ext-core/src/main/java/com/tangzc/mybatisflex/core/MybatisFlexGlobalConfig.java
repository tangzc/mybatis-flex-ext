package com.tangzc.mybatisflex.core;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author don
 */
@Data
@ConfigurationProperties("mybatis-flex-ext")
public class MybatisFlexGlobalConfig {

    private Boolean enableAutoFill = true;
}
